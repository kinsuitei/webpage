'use strict';

module.exports = function( grunt ) {

    //========================================================
    // grunt.initConfig
    //========================================================
    grunt.initConfig({

        //----------------------------------------------------
        // Connect
        //----------------------------------------------------
        connect: {
            server: {
                options: {
                    hostname: 'localhost',
                    port: 35729,
                    base: 'develop'
                }
            }
        },
    
        //----------------------------------------------------
        // watch
        //----------------------------------------------------
        watch: {
            options: {
                livereload: true
            },
            // sass
            sass: {
                files: ['develop/scss/**/*.scss'],
                tasks: ['compass'],
            },
            // haml
            haml: {
                files: ['develop/haml/**/*.haml'],
                tasks: ['haml'],
            }
        },

        //----------------------------------------------------
        // Compass
        //----------------------------------------------------
        compass: {
            build: {
                options: {
                    config: 'config.rb'
                }
            }
        },

        //----------------------------------------------------
        // Haml
        //----------------------------------------------------
        haml: {
            build: {
                options: {
                    language: 'ruby',
                    rubyHamlCommand: 'haml -E UTF-8'
                },
                // dynamic mappings
                expand: true,
                cwd: './develop/haml/',
                //src: ['*.haml'],
                src: ['*.haml'],
                dest: 'develop',
                ext: '.html',
                extDot: 'last'
            }
        }

    });

    //========================================================
    // import plugins from package.json
    //========================================================
    var pkg = grunt.file.readJSON( 'package.json' );
    for ( var taskName in pkg.devDependencies ) {
        if ( taskName.substring( 0, 6 ) == 'grunt-' ) {
            grunt.loadNpmTasks( taskName );
        }
    }

    //========================================================
    // default
    //========================================================
    grunt.registerTask( 'default', ['connect', 'haml', 'compass', 'watch'] );
  
};