# Require any additional compass plugins here.
require 'modular-scale'
 
# Set this to the root of your project when deployed:
#project_path = 'C:\xampp\htdocs\wordpress\wp-content\themes\wp_Kinsuitei'
http_path = "/"
sass_dir = "develop/scss"
css_dir = "develop/css"
javascripts_dir = "develop/js"
images_dir = "develop/img"
fonts_dir = "develop/font"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
output_style = :expanded
 
# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true
 
# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false
 
# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass

# For debug
sass_options = { :debug_info => false }
