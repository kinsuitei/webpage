function initialize() {
var myOptions = {
  zoom: 15,
  center: new google.maps.LatLng(34.924, 135.694),
  mapTypeId: google.maps.MapTypeId.ROADMAP,
  styles: [
  // Administrative
  {
    "featureType": "administrative",
    "stylers": [
      { "visibility": "off" }
    ]
  },
  // Landscape
  {
    "featureType": "landscape",
    "stylers": [
      { "saturation": -100 },
      { "lightness": 65 },
      { "visibility": "on" }
    ]
  },
  // Point of interest
  {
    "featureType": "poi.business",
    "elementType": "geometry.fill",
    "stylers": [
      { "visibility": "on" },
      { "hue": "#ff005d" },
      { "saturation": -10 }
    ]
  },
  {
    "featureType": "poi.school",
    "elementType": "geometry",
    "stylers": [
      { "visibility": "off" }
    ]
  },
  // Road
  {
    "featureType": "road",
    "stylers": [
      { "saturation": -20 },
      { "lightness": 50 }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry.stroke",
    "stylers": [
      { "visibility": "on" },
      { "lightness": -10 }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels",
    "stylers": [
      { "visibility": "off" }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "geometry.fill",
    "stylers": [
      { "visibility": "off" }
    ]
  },
  // Water
  {
    "featureType": "water",
    "elementType": "labels",
    "stylers": [
      { "visibility": "off" },
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      { "visibility": "on" },
      { "hue": "#006eff" },
      { "saturation": -24 }
    ]
  }]
};
var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

var markerOptions = {
  position: new google.maps.LatLng(34.922784, 135.687659),
  map: map,
  title: '錦水亭',
  draggable: false,
  animation: google.maps.Animation.DROP
};
var marker = new google.maps.Marker(markerOptions);

}