$(document).ready(function(){

    var myOptions = {
        noImages: 5,
        path: "img/access/slide/",
        timerInterval: 8000,
        fadeTime: 2000,
	    randomise: false
    };

    // Woo! We have example number 2!
    $('#slide').easySlides(myOptions);

})