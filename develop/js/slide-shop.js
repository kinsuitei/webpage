$(document).ready(function(){

    var myOptions = {
        noImages: 3,
        path: "img/shop/slide/",
        timerInterval: 8000,
        fadeTime: 2000,
	    randomise: false
    };

    // Woo! We have example number 2!
    $('#slide').easySlides(myOptions);

})